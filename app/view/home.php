<div id="welcome">
	<div class="row">
		<div class="col-7 fr">
			<h2>Get Your Home</h2>
			<h3>CLEANED WITH OUR PROFESSIONAL CLEANING SERVICES</h3>
			<div class="container">
				<h4>WELCOME</h4>
				<h1>Kaba <span>Home Cleaning Services</span> </h1>
				<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. </p>

				<p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.</p>

				<a href="about#content" class="btn">LEARN MORE</a>
				<a href="tel:612-294-6526" class="phone"><img src="public/images/common/phone.png" alt="phone icon"><span> 612-294-6526</span></a>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<div id="services">
	<div class="row">
		<h2>OUR SERVICES</h2>
		<div class="container">
			<dl>
				<dt>
					<img src="public/images/content/service1.jpg" alt="Services Image 1">
				</dt>
				<dd>
					<h4>Kitchen Cleaning</h4>
					<ul>
						<li>Countertops</li>
						<li>Sinks</li>
						<li>Appliances</li>
						<li>Stove (everything movable)</li>
						<li>Microwave (in/out)</li>
						<li>Oven (outside)</li>
						<li>Refrigerator (top/front)</li>
						<li>Window (over sink)</li>
						<li>Floor washed</li>
					</ul>
				</dd>
			</dl>
			<dl>
				<dt>
					<img src="public/images/content/service2.jpg" alt="Services Image 2">
				</dt>
				<dd>
					<h4>Bathroom Cleaning</h4>
					<ul>
						<li>Showers (tile, tracks, doors)</li>
						<li>Toilets</li>
						<li>Basins</li>
						<li>Tubs</li>
						<li>Vanity Tops</li>
						<li>Mirrors</li>
						<li>Metal Fixtures</li>
						<li>Floors washed</li>
					</ul>
				</dd>
			</dl>
			<dl>
				<dt>
					<img src="public/images/content/service3.jpg" alt="Services Image 3">
				</dt>
				<dd>
					<h4>Room Cleaning</h4>
					<ul>
						<li>Furniture dusted/polished</li>
						<li>Light fixtures and miniblinds dusted</li>
						<li>Window sills</li>
						<li>Mirrors</li>
						<li>Carpets vacuumed</li>
						<li>Floors washed</li>
						<li>Cobwebs removed</li>
						<li>Fingerprints cleaned</li>
						<li>Trash emptied</li>
						<li>Ceiling fans dusted</li>
					</ul>
				</dd>
			</dl>
			<dl>
				<dt>
					<img src="public/images/content/service4.jpg" alt="Services Image 4">
				</dt>
				<dd>
					<h4>Pressure Washing</h4>
					<ul>
						<li>Fences</li>
						<li>Decks</li>
						<li>Driveways</li>
						<li>Walkways</li>
						<li>Patio</li>
						<li>Brick Houses</li>
						<li>Roof Cleaning</li>
						<li>Power Washing </li>
						<li>Water Damage Clean Up</li>
					</ul>
				</dd>
			</dl>
		</div>
		<a href="services#content" class="btn">View All Services</a>
	</div>
</div>
<div id="experience">
	<div class="row">
		<div class="expLeft col-5 fl">
			<h2>Experience the Difference.</h2>
			<p>Get the kind of clean that only comes from a team of specialists. <br>Integrity, remaining the foundation of our success and wholeness.</p>
			<a href="contact#content" class="btn2">Contact Us Today</a>
		</div>
		<div class="expRight col-7 fr">
			<dl>
				<dt> <img src="public/images/content/exp1.png" alt="Experience Image 1"> </dt>
				<dd>One Time Cleaning</dd>
			</dl>
			<dl>
				<dt> <img src="public/images/content/exp2.png" alt="Experience Image 2"> </dt>
				<dd>Weekly Cleaning</dd>
			</dl>
			<dl>
				<dt> <img src="public/images/content/exp3.png" alt="Experience Image 3"> </dt>
				<dd>Monthly Cleaning</dd>
			</dl>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<div id="works">
	<div class="row">
		<div class="wkLeft col-8 fl">
			<h2>OUR WORKS</h2>
			<div class="container">
				<img src="public/images/content/works1.jpg" alt="Work Image 1">
				<img src="public/images/content/works2.jpg" alt="Work Image 2">
				<img src="public/images/content/works3.jpg" alt="Work Image 3">
				<img src="public/images/content/works4.jpg" alt="Work Image 4">
				<img src="public/images/content/works5.jpg" alt="Work Image 5">
				<img src="public/images/content/works6.jpg" alt="Work Image 6">
				<img src="public/images/content/works7.jpg" alt="Work Image 7">
				<img src="public/images/content/works8.jpg" alt="Work Image 8">
			</div>
		</div>
		<div class="wkRight col-4 fr">
			<div class="container">
				<h1>Contact Us</h1>
				<form action="sendContactForm" method="post"  class="sends-email ctc-form" >
					<input type="text" name="name" placeholder="Name:">
					<input type="text" name="email" placeholder="Email:">
					<input type="text" name="phone" placeholder="Phone:">
					<textarea name="message" cols="30" rows="10" placeholder="Message:"></textarea>
					<input type="checkbox" name="consent" class="consentBox"><p class="terms">I hereby consent to having this website store my submitted information so that they can respond to my inquiry.</p><br>
					<?php if( $this->siteInfo['policy_link'] ): ?>
					<input type="checkbox" name="termsConditions" class="termsBox"/><p class="terms"> I hereby confirm that I have read and understood this website's <a href="<?php $this->info("policy_link"); ?>" target="_blank">Privacy Policy.</a></p>
					<div class="g-recaptcha"></div>
					<?php endif ?>
					<button type="submit" class="ctcBtn btn" disabled>SUBMIT FORM</button>
				</form>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
